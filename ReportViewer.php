<?php
include_once("./include/GoogleMap.php");
include_once("./include/JSMin.php");
class ReportViewer{
    function showMap(){//$latitude, $longitude, $type, $time){

        $MAP_OBJECT = new GoogleMapAPI();
        $MAP_OBJECT->_minify_js = isset($_REQUEST["min"])?FALSE:TRUE;
        //$MAP_OBJECT->setDSN("mysql://user:password@localhost/db_name");
        $MAP_OBJECT->addMarkerByCoords(-75.6026271,6.2199476,"Accident", "Description");
        //function addMarkerByCoords($lon,$lat,$title = '',$html = '',$tooltip = '', $icon_filename = '', $icon_shadow_filename='')
        echo ("<html>
        <head>");

        echo $MAP_OBJECT->getHeaderJS();
        echo $MAP_OBJECT->getMapJS();

        echo ("</head>w
        <body><table><tr><td>");

        echo $MAP_OBJECT->printOnLoad();
        echo $MAP_OBJECT->printMap();
        echo $MAP_OBJECT->printSidebar();
        echo("</td><td>");
        echo "Reports: <br>";
        $reports= simplexml_load_file('reports.xml');
        foreach ($reports->report as $report) {
            echo "Tipo: ".$report['type'];
            echo " Hora: ".date("d M Y H:i:s",(double)$report['time']/1000);
            echo " Latitud: ".$report['latitude'];
            echo " Longitud: ".$report['longitude'];
            echo "<br>";
        }
        //echo $reports->report[0]['time'];
        echo ("</td></tr></table></body>
        </html>");
        return true;
    }
}

$viewer = 'ReportViewer';
//$path_xml='reports.xml';
//$viewer::showReports($path_xml);
$viewer::showMap();
?>


