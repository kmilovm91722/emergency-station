<?php
$path_XML = "../reports.xml";
try{
    if(file_exists($path_XML)){
        header("Access-Control-Allow-Origin: *");
        header("Content-type: text/xml; charset=utf-8");
        $contents=file_get_contents($path_XML, FILE_USE_INCLUDE_PATH);
    }
else{
    throw new Exception('Could not find: '.$path_XML);
}
}catch(Exception $e){
echo $e->getMessage();
}
echo $contents;
?>
