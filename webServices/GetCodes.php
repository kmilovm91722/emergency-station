<?php
$path_XML = "../reports.xml";
$latitude = $_GET['latitude'];
$longitude = $_GET['longitude'];

if(file_exists($path_XML)){
    $reports = new SimpleXMLElement($path_XML, null, true);
    $distances = array();
    foreach($reports->report as $seg){
        $x = pow( ($latitude - $seg['latitude']), 2 );
        $y = pow( ($longitude - $seg['longitude']), 2 );
        $d = sqrt($x + $y);
        $c = (string)$seg['code'];
        $distances[ $c ] =  $d;
    }
    asort($distances);
    $keys = array_keys($distances);
    $result = new SimpleXMLElement("<results></results>");
    $count = 0;
    foreach ($distances as $key => $value) {
        $result->addChild('code', $key);
        $count++;
        if($count==5){
            break;
        }
    }
    header("Content-type: text/xml; charset=utf-8");
    echo $result->asXML();
}
else{
echo 'Could not find: '.$path_XML;
}
?>

