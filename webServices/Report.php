<?php
$path_XML = '../reports.xml';
$code= $_GET['code'];
$time = $_GET['time'];
$latitude = $_GET['latitude'];
$longitude = $_GET['longitude'];
$type = $_GET['type'];
$state = $_GET['state'];

if($code!=null){
    try {
        $reports = new SimpleXMLElement($path_XML, null, true);
        foreach($reports->report as $seg)
        {
            if($seg['code'] == $code) {
                $dom=dom_import_simplexml($seg);
                $dom->parentNode->removeChild($dom);
            }
        }
        header('Content-type: text/xml');
        echo $reports->asXML();
        $reports->asXML($path_XML);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}else{
    try{
        $reports = new SimpleXMLElement($path_XML, null, true);
        $code = uniqid();
        $report = $reports->addChild('report');
        $report->addAttribute("code", $code);
        $report->addAttribute("time", $time);
        $report->addAttribute("latitude", $latitude);
        $report->addAttribute("longitude", $longitude);
        $report->addAttribute("type", $type);

        header('Content-type: text/xml');
        echo $reports->asXML();
        $reports->asXML($path_XML);
    }catch(Exception $e){
        echo $e->getMessage();
    }
}
?>
